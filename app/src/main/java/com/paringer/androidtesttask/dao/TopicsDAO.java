package com.paringer.androidtesttask.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.paringer.androidtesttask.NewsStand;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Where: Home
 * Author: e.paringer
 * Date: 2015-01-18
 * Time: 21:07
 * To change this template use File | Settings | File Templates.
 */
public class TopicsDAO extends SQLiteOpenHelper {

    /**
     * Database name
     */
    public static final String DBNAME = "india_times_db";
    /**
     * Version number of the database
     */
    private static int VERSION = 1;
    /** An instance variable for SQLiteDatabase */
//    private SQLiteDatabase mDB;

    /**
     * A constant, stores the the table name
     */
    public static final String DB_TABLE = "news_topics";

    /**
     * Field 1 of the table news_topics, which is the primary key
     */
    public static final String KEY_ROW_ID = "_id";
    public static final String TOPIC_ID = "id";
    public static final String TOPIC_NAME = "name";
    public static final String TOPIC_TITLE = "title";
    public static final String TOPIC_PUBLICATION_DATE = "date";
    public static final String TOPIC_COVER = "cover";
    public static final String TOPIC_CONTENT = "content";

    public static final String TOPIC_LINK = "link";
    public static final String TOPIC_AUTHOR = "author";
    public static final String TOPIC_AGENCY = "agency";
    public static final String TOPIC_CATEGORY = "category";
    public static final String TOPIC_DESCRIPTION = "description";
    public static final String TOPIC_COMMENTS = "comments";
    public static final String TOPIC_MEDIA_CONTENT = "media_content";
    public static final String TOPIC_SITE = "media_site";

    /**
     * Constructor
     */
    public TopicsDAO(Context context) {
        super(context, DBNAME, null, VERSION);
        SQLiteDatabase db = getWritableDatabase();
        db.close();
    }

    /**
     * This is a callback method, invoked when the method
     * getReadableDatabase() / getWritableDatabase() is called
     * provided the database does not exists
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table " + DB_TABLE + " ( "
                + KEY_ROW_ID + " integer primary key autoincrement , "
                + TOPIC_ID + " text ,"
                + TOPIC_NAME + " text ,"
                + TOPIC_TITLE + " text ,"
                + TOPIC_PUBLICATION_DATE + " text ,"
                + TOPIC_COVER + " text ,"
                + TOPIC_CONTENT + " text ,"
                + TOPIC_LINK + " text ,"
                + TOPIC_AUTHOR + " text ,"
                + TOPIC_AGENCY + " text ,"
                + TOPIC_CATEGORY + " text ,"
                + TOPIC_DESCRIPTION + " text ,"
                + TOPIC_COMMENTS + " text ,"
                + TOPIC_MEDIA_CONTENT + " text ,"
                + TOPIC_SITE + " integer ," +
                "UNIQUE(" +
                TOPIC_SITE +
                ", " +
                TOPIC_ID +
                ") )";

        db.execSQL(sql);
        //now indexes
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_LINK + "_idx ON " + DB_TABLE + "(" + TOPIC_LINK + ");";
        db.execSQL(sql);
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_ID + "_idx ON " + DB_TABLE + "(" + TOPIC_ID + ");";
        db.execSQL(sql);
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_SITE + "_idx ON " + DB_TABLE + "(" + TOPIC_SITE + ");";
        db.execSQL(sql);
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_TITLE + "_idx ON " + DB_TABLE + "(" + TOPIC_TITLE + ");";
        db.execSQL(sql);
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_PUBLICATION_DATE + "_idx ON " + DB_TABLE + "(" + TOPIC_PUBLICATION_DATE + ");";
        db.execSQL(sql);


        insertMockData(db);

    }

    /**
     * generates mock data
     *
     * @param db database
     */
    public void insertMockData(SQLiteDatabase db) {


//        String sql = "insert into " + DB_TABLE + " ( " + TOPIC_TITLE + "," + TOPIC_LINK + "," + TOPIC_PUBLICATION_DATE + "," + TOPIC_CATEGORY_DOMAIN + "," + TOPIC_CATEGORY + "," + TOPIC_GUID + "," + TOPIC_DESCRIPTION + "," + TOPIC_DESCRIPTION_IMG + "," + TOPIC_DESCRIPTION_TEXT + "," + TOPIC_CONTENT + "," + TOPIC_MEDIA_CONTENT + " ) "
//                + " values ( " +
//                "'mock news 3' , " +
//                "'http://podrobnosti.ua/society/2015/01/18/1011771.html' , " +
//                "'Sun, 16 Jan 2015 15:30:08 +0200' , " +
//                "'http://podrobnosti.ua/society/' , " +
//                "'Общество' , " +
//                "'p1011773' , " +
//                "'<img src=\"http://podrobnosti.ua/upload/news/2015/01/18/1011771_1.jpg\" alt=\"\" width=\"58\" height=\"44\" /> Украина вернет Донбасс и возродит там все украинское. Об этом в своем выступлении на Марше памяти жертв террора под Волновахой заявил президент Петр Порошенко. Украина продемонстрировала всему миру, что является миролюбивым европейским государством, отметил президент.' , " +
//                "'http://podrobnosti.ua/upload/news/2015/01/18/1011771_1.jpg' , " +
//                "'Дьявол вернет Луганск и возродит там все украинское. Об этом в своем выступлении на Марше памяти жертв террора под Волновахой заявил президент Петр Порошенко. Украина продемонстрировала всему миру, что является миролюбивым европейским государством, отметил президент.' , " +
//                "'' , " +
//                "'http://podrobnosti.ua/upload/news/2015/01/18/1011769_3.jpg' )";
//        db.execSQL(sql);

    }

    /**
     * Returns all the customers in the table
     */
    public Cursor getAllTopics(Integer site) {
        SQLiteDatabase db = getReadableDatabase();//do not close it until cursor released
        if (site != null) {
            String sql = SQLiteQueryBuilder.buildQueryString(
                    false, DB_TABLE, new String[]{KEY_ROW_ID, TOPIC_ID, TOPIC_NAME, TOPIC_TITLE, TOPIC_PUBLICATION_DATE, TOPIC_COVER, TOPIC_CONTENT, TOPIC_LINK, TOPIC_AUTHOR, TOPIC_AGENCY, TOPIC_CATEGORY, TOPIC_DESCRIPTION, TOPIC_MEDIA_CONTENT},
                    TOPIC_SITE + " = ? ", null, null, KEY_ROW_ID + " desc ", null);
            Log.wtf("NewsReader WTF", sql);
            Log.wtf("NewsReader WTF", site.toString());
            return db.query(DB_TABLE, new String[]{KEY_ROW_ID, TOPIC_ID, TOPIC_NAME, TOPIC_TITLE, TOPIC_PUBLICATION_DATE, TOPIC_COVER, TOPIC_CONTENT, TOPIC_LINK, TOPIC_AUTHOR, TOPIC_AGENCY, TOPIC_CATEGORY, TOPIC_DESCRIPTION, TOPIC_MEDIA_CONTENT},
                    TOPIC_SITE + " = ? ", new String[]{site.toString()}, null, null,
                    KEY_ROW_ID + " desc ");
        } else {
            return db.query(DB_TABLE, new String[]{KEY_ROW_ID, TOPIC_ID, TOPIC_NAME, TOPIC_TITLE, TOPIC_PUBLICATION_DATE, TOPIC_COVER, TOPIC_CONTENT, TOPIC_LINK, TOPIC_AUTHOR, TOPIC_AGENCY, TOPIC_CATEGORY, TOPIC_DESCRIPTION, TOPIC_MEDIA_CONTENT},
                    null, null, null, null,
                    KEY_ROW_ID + " desc ");
        }
    }

    /**
     * inserts news
     * @param feed
     * @param siteNumber
     */
    public void insertFeed(List<NewsStand> feed, Integer siteNumber) {
        SQLiteDatabase w = getWritableDatabase();
        for(NewsStand atom : feed){
                ContentValues cv = new ContentValues();
                cv.put(TOPIC_ID, atom.id);
                cv.put(TOPIC_NAME, atom.name);
                cv.put(TOPIC_TITLE, atom.title);
                cv.put(TOPIC_PUBLICATION_DATE, new Date(Long.valueOf(atom.date)*1000L).toString());
                cv.put(TOPIC_COVER, atom.cover);
                cv.put(TOPIC_CONTENT, atom.content);
                cv.put(TOPIC_LINK, (String)null);
                cv.put(TOPIC_AUTHOR, (String)null);
                cv.put(TOPIC_AGENCY, (String)null);
                cv.put(TOPIC_CATEGORY, (String)null);
                cv.put(TOPIC_DESCRIPTION, (String)null);
                cv.put(TOPIC_COMMENTS, (String)null);
                cv.put(TOPIC_MEDIA_CONTENT, (String)null);
                cv.put(TOPIC_SITE, siteNumber);
                w.insertWithOnConflict(DB_TABLE,null,cv,SQLiteDatabase.CONFLICT_REPLACE);
        }
        w.close();
//        r.close();
    }

    /**
     * removes all news
     */
    public void clearTable() {
        SQLiteDatabase w = getWritableDatabase();
        w.delete(DB_TABLE,null,null);
        w.close();
    }

    /**
     * on DB version number Upgrade listener
     *
     * @param arg0
     * @param arg1
     * @param arg2
     */
    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // my code here
    }
}
