package com.paringer.androidtesttask;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.paringer.androidtesttask.dao.TopicsDAO;
import com.paringer.androidtesttask.dao.TopicsLoader;
import com.paringer.androidtesttask.dao.TopicsProvider;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.androidalliance.edgeeffectoverride.ListView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String SITE_URL = "http://newsstand.4elements.kiev.ua/"; //"http://newsstand.4elements.kiev.ua/repository/testJson.plist";
    private SimpleCursorAdapter mSimpleCursorAdapter;
    private TopicsLoader mTopicsLoader;
    private TopicsDAO mDbHelper;
    private ActionMode mActionMode;
    private DialogContext dlg1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"try to feed me |:]");
                refreshing();
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        init();
    }

    public void init(){
        mDbHelper = new TopicsDAO(this);
        mDbHelper.getWritableDatabase().close();
        mSimpleCursorAdapter = new SimpleCursorAdapter(this, R.layout.topic_item, null,
                new String[]{TopicsDAO.TOPIC_ID,
                        TopicsDAO.TOPIC_NAME,
                        TopicsDAO.TOPIC_TITLE,
                        TopicsDAO.TOPIC_COVER,
                        TopicsDAO.TOPIC_CONTENT,
                        TopicsDAO.TOPIC_PUBLICATION_DATE},
                new int[]{R.id.id,
                        R.id.name,
                        R.id.title, R.id.cover, R.id.content, R.id.date},
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        ) {
            @Override
            public void setViewImage(ImageView v, String value) {
                Glide
                        .with(MainActivity.this)
                        .load(value)
                        .placeholder(android.R.drawable.ic_menu_view)
                        .into(v);
            }
        };
        ListView list = (ListView) findViewById(R.id.listview);
        list.setAdapter(mSimpleCursorAdapter);
//        this.mRefreshLayout.setOnRefreshListener(this);
        dlg1 = DialogContext.newInstance("","");

        /** Creating a loader for populating listview from sqlite database */
        /** This statement, invokes the method onCreatedLoader() */
        mTopicsLoader = new TopicsLoader(this, mSimpleCursorAdapter);
        this.getLoaderManager().initLoader(0, null, mTopicsLoader);
//        mSimpleCursorAdapter.notifyDataSetChanged();
//        reloadAndRefresh(mList);
        list.setOnTouchListener(new OnSwipeTouchListener(list) {
            @Override
            public boolean onSwipeTop(int position) {
//                Toast.makeText(MainActivity.this, "top", Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onSwipeRight(int position) {
//                Toast.makeText(MainActivity.this, "right", Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onSwipeLeft(int position) {
//                Toast.makeText(MainActivity.this, "left", Toast.LENGTH_SHORT).show();
                // Start the CAB using the ActionMode.Callback defined above
//                mActionMode = MainActivity.this.startActionMode(mActionModeCallback);
//                view.setSelected(true);
                dlg1.show(getFragmentManager(),DialogContext.TAG);
                return true;
            }

            @Override
            public boolean onSwipeBottom(int position) {
//                Toast.makeText(MainActivity.this, "bottom", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
//        registerForContextMenu(list);
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_edit:
//                editNote(info.id);
                edit();
                return true;
            case R.id.menu_close:
//                deleteNote(info.id);
                close();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_edit:
//                    shareCurrentItem();
                    edit();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.menu_close:
//                    undoCurrentItem();
                    close();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };
    public void edit(){
        Toast.makeText(this, getCountryCode(), Toast.LENGTH_SHORT).show();
    }

    public void close(){
        CodeNotification.notify(this,getCountryCode(),1);
    }

    public String getCountryCode(){
        TelephonyManager tm = (TelephonyManager)this.getSystemService(this.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        if("US".equalsIgnoreCase(countryCodeValue)){
            countryCodeValue = this.getResources().getConfiguration().locale.getCountry();
        }
        return countryCodeValue;
    }

    /**
     * refresh and reload list content of news topics list
     */
    public void refreshing() {
        // do something when refresh starts

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(SITE_URL)  //call your base url
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        NewsStandSite site = restAdapter.create(NewsStandSite.class);
//        Callback<ResponseBody> callback = new Callback<ResponseBody>();
        retrofit2.Callback<List<NewsStand> > callback = new retrofit2.Callback<List<NewsStand> >() {
            @Override
            public void onResponse(Call<List<NewsStand>> call, retrofit2.Response<List<NewsStand> > response) {
//                mRefreshLayout.finishRefreshing();
//                Log.d("WTF OK", "" + response.message());
//                Log.d("WTF OK", "" + response.code());
//                Log.d("WTF OK", "" + response.body());
                try {
                    Log.d(TAG, "my err body:" + (response.errorBody() != null ? response.errorBody().string() : "null"));
                } catch (IOException e) {
                }
                for (NewsStand n : response.body()) {
                    Log.d(TAG, n.title);
                    Log.d(TAG, "\n");
                }
                mDbHelper.insertFeed(response.body(), Integer.valueOf(TopicsProvider.CURRENT_FEED_SHORT_NAME.hashCode()));
                refreshList();
            }

            @Override
            public void onFailure(Call<List<NewsStand> > call, Throwable t) {
//                mRefreshLayout.finishRefreshing();
                refreshList();
                Log.e(TAG, "" + t);
            }
        };
        retrofit2.Call<List<NewsStand> > feedNews = site.feed(); // ghService.listRepos("paringer", callback);
        feedNews.enqueue(callback);
    }

    /**
     * refresh list content of news topics list
     */
    public void refreshList() {
        ListView list = (ListView) findViewById(R.id.listview);
        this.getLoaderManager().restartLoader(0, null, mTopicsLoader);//new TopicsLoader(mActivity, mSimpleCursorAdapter)
        list.deferNotifyDataSetChanged();
        list.setAdapter(list.getAdapter());
//        mList.scrollTo(0,0);
        list.invalidate();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camara) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
