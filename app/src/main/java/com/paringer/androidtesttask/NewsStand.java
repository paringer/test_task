package com.paringer.androidtesttask;

import java.util.Date;

/**
 * Created by Zhenya on 01.04.2016.
 */
public class NewsStand {
    public Integer id;
    public String name;
    public String title;
    public String date;
    public String cover;
    public String content;

    @Override
    public String toString() {
        return "{ " + id
             + ", " + name
             + ", " + title
             + ", " + date
             + ", " + cover
             + ", " + content
             + "}";
    }
}
