package com.paringer.androidtesttask;

import android.app.DialogFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DialogContext.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DialogContext#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DialogContext extends DialogFragment implements ImageView.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = "DialogContext";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DialogContext() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DialogContext.
     */
    // TODO: Rename and change types and number of parameters
    public static DialogContext newInstance(String param1, String param2) {
        DialogContext fragment = new DialogContext();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("menu");
        View v = inflater.inflate(R.layout.dialog_context_menu, null);
        ImageView edit = (ImageView) v.findViewById(R.id.imageViewEdit);
        ImageView close = (ImageView) v.findViewById(R.id.imageViewClose);
        edit.setOnClickListener(this);
        close.setOnClickListener(this);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        ImageView edit = (ImageView) v.findViewById(R.id.imageViewEdit);
        ImageView close = (ImageView) v.findViewById(R.id.imageViewClose);
        if (edit==v){
            edit();
        }
        if(close==v){
            close();
        }
        dismiss();
    }

    public void edit(){
        Toast.makeText(getActivity(), getCountryCode(), Toast.LENGTH_SHORT).show();
    }
    public void close(){
        CodeNotification.notify(getActivity(), getCountryCode(), 1);
    }
    public String getCountryCode(){
        TelephonyManager tm = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        if("US".equalsIgnoreCase(countryCodeValue)){
            countryCodeValue = this.getResources().getConfiguration().locale.getCountry();
        }
        return countryCodeValue;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
