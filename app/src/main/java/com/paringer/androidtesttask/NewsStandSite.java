package com.paringer.androidtesttask;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Zhenya on 01.04.2016.
 */
public interface NewsStandSite {
        @GET("repository/testJson.plist")
        Call<List<NewsStand>> feed();//@Path("user") String user
}
