package com.paringer.androidtesttask;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import uk.co.androidalliance.edgeeffectoverride.ListView;

public class OnSwipeTouchListener implements View.OnTouchListener {

    private final GestureDetector gestureDetector = new GestureDetector(new GestureListener());
    private final ListView mListView;

    public OnSwipeTouchListener(ListView listView) {
        this.mListView = listView;
    }

    public boolean onTouch(final View v, final MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return super.onDown(e);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            int position = ListView.INVALID_POSITION;
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
//                        Toast.makeText(listAdapter.getItem( mListView.pointToPosition( (int) e1.getX(), (int) e1.getY() ).toString() );
                        position = mListView.pointToPosition((int) e1.getX(), (int) e1.getY());
                        if (diffX > 0) {
                            result = onSwipeRight(position);
                        } else {
                            result = onSwipeLeft(position);
                        }
                    }
                } else {
//                    Toast.makeText(listAdapter.getItem( mListView.pointToPosition( (int) e1.getX(), (int) e1.getY() ).toString() );
                    position = mListView.pointToPosition((int) e1.getX(), (int) e1.getY());
                    if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            result = onSwipeBottom(position);
                        } else {
                            result = onSwipeTop(position);
                        }
                    }
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    public boolean onSwipeRight(int position) {
        return false;
    }

    public boolean onSwipeLeft(int position) {
        return false;
    }

    public boolean onSwipeTop(int position) {
        return false;
    }

    public boolean onSwipeBottom(int position) {
        return false;
    }
}